function proxyShield(fn) {
    const origProp = window.ethereum.request;
    window.ethereum.request = (...args) => {
        fn("etherium request", args)
        return Reflect.apply(origProp, window.ethereum, args);
  };
}

const handleMethodCall = (fnName, fnArgs) => console.log(`${fnName} called with `, fnArgs);

proxyShield(handleMethodCall);

alert("we got this");